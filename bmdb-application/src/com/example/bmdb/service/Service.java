package com.example.bmdb.service;
import java.util.List;

import com.example.bmdb.domain.Builder;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;

public class Service {
	Builder builder;
	
	public Service(Builder builder)
	{
		this.builder=builder;
	}

	public void saveUser(User user) {
		
	}

	public User findUser(int idx) {
		return this.builder.getUsers().get(idx);
	}

	public List<Media> findAllMedia() {
		return builder.getMedias();
	}

	public void saveReview(Media media, Review review) {
	}

	public List<Review> findAllReview(Media media) {
		return builder.getReviews();
	}

}
