package com.example.bmdb.view;
import com.example.bmdb.domain.Builder;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.User;

public class View {
	Builder builder;
	public View(Builder builder)
	{
		this.builder=builder;
	}
	public User readUserData() {
		return null;
	}

	public void printWelcomeMessage() {
		System.out.println("Welcome to the movie and series review application ");
}

	public void printMedias() {
		System.out.println("[ Medias ]");
		for (Media item : builder.getMedias()) {
			System.out.println(item);
			item.ActorList();
			System.out.println("");
		}
		System.out.println("");
	}

	public void printReviews(User user) {

	}

}
