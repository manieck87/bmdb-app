package com.example.bmdb.domain;

public final class Review {
	private String text;
	private User userReview;
	private Media mediaReview;
	private Rating ratingReview;

	public Review(String text, User user, Media media, Rating rating) {
		this.text = text;
		this.userReview = user;
		this.mediaReview = media;
		this.ratingReview = rating;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUserReview() {
		return userReview;
	}

	public void setUserReview(User userReview) {
		this.userReview = userReview;
	}

	public Media getMediaReview() {
		return mediaReview;
	}

	public void setMediaReview(Media mediaReview) {
		this.mediaReview = mediaReview;
	}

	public Rating getRatingReview() {
		return ratingReview;
	}

	public void setRatingReview(Rating ratingReview) {
		this.ratingReview = ratingReview;
	}

	@Override
	public String toString() {
		return  "' [" + text + "] " + mediaReview + " Rating= [ " + ratingReview + " ]";
	}

}
