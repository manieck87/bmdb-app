package com.example.bmdb.domain;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Builder {
	private List<User> users;
	List<Actor> actors;
	private List<Review> reviews;
	private List<Media> medias;

	public Builder() {
		this.actors = new ArrayList<>();
		this.setUsers(new ArrayList<>());
		this.setReviews(new ArrayList<>());
		this.setMedias(new ArrayList<>());
		Build();
	}

	public void PrintDebugActors() {
		System.out.println("[ Actors ]");
		for (Actor item : actors) {
			System.out.println(item);
			item.FilmList();
			System.out.println("");
		}
		System.out.println("");
	}

	public void PrintDebugMedias() {
		System.out.println("[ Medias ]");
		for (Media item : getMedias()) {
			System.out.println(item);
			item.ActorList();
			item.ReviewList();
			System.out.println("");
		}
		System.out.println("");

	}

	public void PrintDebugUsers() {
		System.out.println("[ Users ]");
		for (User item : getUsers())
			System.out.println(item);
		System.out.println("");
	}

	public void PrintDebugReviews() {
		System.out.println("[ Reviews ]");
		for (Review item : getReviews())
			System.out.println(item);
		System.out.println("");
	}

	public void Build() {
		Actor jc = new Actor("Jim Carrey", LocalDate.parse("1962-01-17"), Sex.MALE, "Jim Carrey, Canadian-born and a U.S. citizen since 2004, is an actor and producer famous for his rubbery body movements and flexible facial expressions. The two-time Golden Globe-winner rose to fame as a cast member of the Fox sketch comedy In Living Color (1990) but leading roles in Ace Ventura: Pet Detective (1994), Dumb and Dumber (1994) and The Mask (1994) established him as a bankable comedy actor.");
		Actor al = new Actor("Andrew Lincoln", LocalDate.parse("1973-09-14"), Sex.MALE, "Andrew Lincoln is a British actor. Lincoln spent his early childhood in Hull, Yorkshire before his family relocated to Bath, Somerset when he was age 10. He was educated at Beechen Cliff School in Bath, and then the prestigious Royal Academy of Dramatic Art in London. His father is a civil engineer and his mother is a nurse");
		Actor mm = new Actor("Melissa McBride", LocalDate.parse("1965-05-23"), Sex.FEMALE, "Melissa Suzanne McBride (born May 23, 1965) is an American actress and former casting director, best known for her role as Carol Peletier on the AMC series The Walking Dead. McBride has garnered critical acclaim and received multiple awards and nominations for her role on the show.");
		Actor nr = new Actor("Norman Reedus", LocalDate.parse("1969-01-06"), Sex.MALE, "Bla bla bla");
		Actor dg = new Actor("Danai Gurira", LocalDate.parse("1978-02-14"), Sex.FEMALE, "Bla bla bla");
		Actor mr = new Actor("Margot Robbie", LocalDate.parse("1990-07-02"), Sex.FEMALE, "Bla bla bla");
		Actor em = new Actor("Ewan McGregor", LocalDate.parse("1971-03-31"), Sex.MALE, "Bla bla bla");
		Actor pp = new Actor("Pedro Pascal", LocalDate.parse("1962-01-17"), Sex.MALE, "Bla bla bla");
		Actor cw = new Actor("Carl Weathers", LocalDate.parse("1948-01-14"), Sex.MALE, "Bla bla bla");
		Actor ec = new Actor("Emilia Clarke", LocalDate.parse("1986-10-23"), Sex.FEMALE, "Bla bla bla");
		actors.add(jc);
		actors.add(al);
		actors.add(mm);
		actors.add(nr);
		actors.add(dg);
		actors.add(mr);
		actors.add(em);
		actors.add(pp);
		actors.add(cw);
		actors.add(ec);
		System.out.println("Actors initialized");
//		for (Actor item : actors) {
//			System.out.println(item);
//			item.FilmList();
//		}
//		System.out.println("");
		Series thm = new Series(new BigDecimal("1"), "The Mandalorian", "The travels of a lone bounty hunter in the outer reaches of the galaxy, far from the authority of the New Republic. A Star Wars series",
				LocalDate.parse("2019-11-01"));
		Series twd = new Series(new BigDecimal("2"), "The Walking Dead", "Sheriff Deputy Rick Grimes wakes up from a coma to learn the world is in ruins, and must lead a group of survivors to stay alive.  Zombie series",
				LocalDate.parse("2007-03-01"));
		Series tgt = new Series(new BigDecimal("3"), "Game of Thrones", "Nine noble families fight for control over the mythical lands of Westeros, while an ancient enemy returns after being dormant for thousands of years.  Dragons and xxx series",
				LocalDate.parse("2011-01-01"));
		Movie obw = new Movie(new BigDecimal("4"), "Obi Wan", "Soon tm Star Wars movie", LocalDate.now().plusDays(100));
		Movie snc = new Movie(new BigDecimal("5"), "Sonic the Hedgehog", "After discovering a small, blue, fast hedgehog, a small-town police officer must help it defeat an evil genius who wants to do experiments on it.  Videogame adaptation live action movie",
				LocalDate.parse("2020-01-13"));
		Movie bop = new Movie(new BigDecimal("6"), "Birds of Prey", "After splitting with the Joker, Harley Quinn joins superheroes Black Canary, Huntress and Renee Montoya to save a young girl from an evil crime lord. Don't watch it", LocalDate.parse("2020-02-10"));
		getMedias().add(thm);
		getMedias().add(twd);
		getMedias().add(tgt);
		getMedias().add(obw);
		getMedias().add(snc);
		getMedias().add(bop);
		System.out.println("Medias initialized");
//		for (Media item : medias)
//			System.out.println(item);
//		System.out.println("");

		thm.AddActor(pp);
		thm.AddActor(cw);
		twd.AddActor(al);
		twd.AddActor(mm);
		twd.AddActor(nr);
		twd.AddActor(dg);
		tgt.AddActor(pp);
		tgt.AddActor(ec);
		obw.AddActor(em);
		snc.AddActor(jc);
		bop.AddActor(em);
		bop.AddActor(mr);

		jc.addFilmography(snc);
		al.addFilmography(twd);
		mm.addFilmography(twd);
		nr.addFilmography(twd);
		dg.addFilmography(twd);
		mr.addFilmography(bop);
		em.addFilmography(bop);
		em.addFilmography(obw);
		pp.addFilmography(tgt);
		pp.addFilmography(thm);
		cw.addFilmography(thm);
		ec.addFilmography(tgt);
		System.out.println("Actors-Medias Association initialized");

		;
		getUsers().add(new User("Mike Hunt", "mikehunt@gmail.com", "mike12345"));
		getUsers().add(new User("Sam Sung", "samsung@gmail.com", "samsung987"));
		getUsers().add(new User("Chris P. Bacon", "ChrisPBacon@gmail.com", "chrisPBacon666"));
		getUsers().add(new User("Charity Beaver", "CharityB@gmail.com", "CharB34v3r"));
		System.out.println("Users initialized");

		//reviews.add(new Review("Bla bla++", users.get(0), medias.get(0), Rating.GOOD));
		Review r1=new Review("Bla bla++", getUsers().get(0), getMedias().get(0), Rating.GOOD);
		Review r2=new Review("Bla + Bla+", getUsers().get(0), getMedias().get(1), Rating.GOOD);
		Review r3=new Review("Bla bla bla", getUsers().get(1), getMedias().get(2), Rating.GOOD);
		Review r4=new Review("Bla bla bla bla", getUsers().get(2), getMedias().get(3), Rating.AVERAGE);
		Review r5=new Review("Kek", getUsers().get(2), getMedias().get(4), Rating.AVERAGE);
		Review r6=new Review("Booo", getUsers().get(3), getMedias().get(5), Rating.BAD);
		getReviews().add(r1);
		getReviews().add(r2);
		getReviews().add(r3);
		getReviews().add(r4);
		getReviews().add(r5);
		getReviews().add(r6);
		getMedias().get(0).AddReview(r1);
		getMedias().get(1).AddReview(r2);
		getMedias().get(2).AddReview(r3);
		getMedias().get(3).AddReview(r4);
		getMedias().get(4).AddReview(r5);
		getMedias().get(5).AddReview(r6);
				
		System.out.println("Reviews initialized");
		System.out.println("");
	}

	public List<Media> getMedias() {
		return medias;
	}

	public void setMedias(List<Media> medias) {
		this.medias = medias;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}
}
