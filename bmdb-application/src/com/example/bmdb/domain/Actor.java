package com.example.bmdb.domain;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public final class Actor {

	private String name;
	private LocalDate born;
	private Sex sexActor;
	private String biography;
	private List<Media> filmography;

	Actor(String name, LocalDate born, Sex sex, String biography) {
		this.name = name;
		this.born = born;
		this.sexActor = sex;
		this.biography = biography;
		this.filmography = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBorn() {
		return born;
	}

	public void setBorn(LocalDate born) {
		this.born = born;
	}

	public Sex getSexActor() {
		return sexActor;
	}

	public void setSexActor(Sex sexActor) {
		this.sexActor = sexActor;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public List<Media> getFilmography() {
		return filmography;
	}

	public void setFilmography(List<Media> filmography) {
		this.filmography = filmography;
	}

	@Override
	public String toString() {
		return "name= " + name + ", born= " + born;// + " bio=[" + biography + "]";// + " sex:" 
	}

	// move to somewhere else below
	public void addFilmography(Media media) {
		filmography.add(media);
	}

	public void FilmList() {
		for (Media item : filmography)
			System.out.println(item);
	}

}
