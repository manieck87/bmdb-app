package com.example.bmdb.domain;
import java.util.ArrayList;
import java.util.List;

public final class User {
	private String name;
	private String email;
	private String password;
	private List<Review> reviewslist;

	public User(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.reviewslist = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Review> getReviewslist() {
		return reviewslist;
	}

	public void setReviewslist(List<Review> reviewslist) {
		this.reviewslist = reviewslist;
	}

	@Override
	public String toString() {
		return name; //+ " " + email + " " + password;// reviewlist
	}

	//somewhere else
	public void AddReview(Review review) {
		reviewslist.add(review);
	}

	public void ReviewList() {
		for (Review item : reviewslist)
			System.out.println(item);
	}
}
