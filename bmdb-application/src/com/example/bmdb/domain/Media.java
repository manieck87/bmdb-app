package com.example.bmdb.domain;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
//import java.time.temporal.ChronoUnit;//
import java.util.List;

public class Media { // abstract?

	private BigDecimal id;
	private String title;
	private String description;
	private LocalDate premier;
	private List<Review> reviewslist;
	private List<Actor> castlist;

	public Media(BigDecimal id, String title, String description, LocalDate premier) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.premier = premier;
		this.reviewslist = new ArrayList<>();
		this.castlist = new ArrayList<>();

	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getPremier() {
		return premier;
	}

	public void setPremier(LocalDate premier) {
		this.premier = premier;
	}

	public List<Review> getReviewslist() {
		return reviewslist;
	}

	public void setReviewslist(List<Review> reviewslist) {
		this.reviewslist = reviewslist;
	}

	public List<Actor> getActorslist() {
		return castlist;
	}

	public void setActorslist(List<Actor> actorslist) {
		this.castlist = actorslist;
	}

	@Override
	public String toString() {
		return id + ": '" + title + "' Description= '" + description + "' Premier=" + premier;// ,reviews,actors
	}

	// somewhere else
	public void AddReview(Review review) {
		reviewslist.add(review);
	}

	//
	public void AddActor(Actor actor) {
		castlist.add(actor);
	}

//
	public void ReviewList() {
		System.out.println("Reviews= [ ");
		for (Review item : reviewslist)
			System.out.println("    "+reviewslist.indexOf(item) + " " + item);
		System.out.println(" ]");

	}

//
	public void ActorList() {
		System.out.println("Cast= [ ");
		for (Actor item : castlist)
			System.out.println("   " + item);
		System.out.println(" ]");

	}
}
