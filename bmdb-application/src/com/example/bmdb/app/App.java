package com.example.bmdb.app;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.bmdb.domain.Builder;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;

import java.io.*;

public class App {
	Review review;
	List<Media> medias;
	Media selectedMedia;

	Service services;
	View views;
	static Builder builder;
	User user;

	App(Service service, View view) {
		builder = new Builder();
		this.user = null;
		this.review = null;
		this.medias = new ArrayList<>();
		this.selectedMedia = null;

		this.services = new Service(builder);
		this.views = new View(builder);
	}

	public void play() {
		Scanner scanner = null;
		Boolean exit = false;
		// System.out.println("What is your name? ");
		// scanner = new Scanner(System.in);
		// String guest = scanner.nextLine();
		// scanner.close();
//		System.out.println("Welcome " + guest + " to the movie and series review application ");
		while (!exit) {
			views.printWelcomeMessage();
			System.out.println("");
			System.out.println("Menu: ");
			System.out.println("1: Create User");
			System.out.println("2: Do a review ");
			System.out.println("3: View PrintMedias ");
			System.out.println("4: View PrintReviewUser ");
			System.out.println("5: View ReadUserData ");
			System.out.println("6: Print Actors ");
			System.out.println("7: Print Medias ");
			System.out.println("8: Print Users ");
			System.out.println("9: Print Reviews ");
			System.out.println("0: Exit from the menu ");
			System.out.println("");
			System.out.println("Choose from the menu: ");

			scanner = new Scanner(System.in);
			String menu = scanner.nextLine();
			switch (menu) {
			case "1":
				createUser();
				break;
			case "2":
				doReview();
				break;
			case "3":
				views.printMedias();
				break;
			case "4":
				views.printReviews(user);
				break;
			case "5":
				views.readUserData();
				break;
			case "6":
				builder.PrintDebugActors();
				break;
			case "7":
				builder.PrintDebugMedias();
				break;
			case "8":
				builder.PrintDebugUsers();
				break;
			case "9":
				builder.PrintDebugReviews();
				break;
			case "0":
				System.out.println("Exit Menu ");
				exit = true;
				break;
			default:
				System.out.println("Error incorrect input");
			}
		}
		scanner.close();
	}

	public void createUser() {
		System.out.print("Enter your name: ");
		Scanner scanner = new Scanner(System.in);
		String name = scanner.nextLine();
		System.out.print("Enter your email: ");
		scanner.close();
		scanner = new Scanner(System.in);
		String email = scanner.nextLine();
		System.out.print("Enter your password: ");
		scanner.close();
		scanner = new Scanner(System.in);
		String password = scanner.nextLine();
		scanner.close();

		// User user = new User(name, email, password);
		user = new User(name, email, password);

		if (builder.getUsers().contains(user)) {
			System.out.println("Email already exist");
		} else {
			// builder.users.add(user);
			services.saveUser(user);
			System.out.println("User created " + user.getName() + " " + user.getEmail());

		}
		// scanner.close();

	}

	public void doReview() {
		// megvan a user vagy a neve
		// kiv�lasztja a medi�t amit �rt�kelni akar
		Media media = medias.get(0);
		System.out.print("Type in your review: ");
		Scanner scanner = new Scanner(System.in);
		String text = scanner.nextLine();
		System.out.print("Rate the movie or series: ");
		scanner = new Scanner(System.in);
		int rating = scanner.nextInt();
		Review newreview = new Review(text, user, media, Rating.GOOD);// a ratinget m�g meg kell oldani
		services.saveReview(media, newreview);

	}

	public void printReviewAverage() {
		System.out.println("Will do something one day");
	}

	public static void main(String[] args) {
		App app = new App(new Service(builder), new View(builder));
		app.play();
		System.out.println("Application ended!");
	}
}

/*
 * Amikor egy Review p�ld�ny beker�l a User list�j�ba, gondoskodni kell r�la,
 * hogy a Review p�ld�ny is tartalmazza a referenci�t arra a User objektumra
 * (hogy k�t ir�ny� legyen az asszoci�ci�)
 * 
 * - K�sz�ts�tek el a Java oszt�lyokat, �s kapcsolataikat - K�sz�tsetek egy
 * met�dust, amely l�trehoz tesztadatokat ezekb�l az oszt�lyokb�l 
 *  - A tesztadatok l�trehoz�s�hoz haszn�lj�tok a Builder tervez�si
 * mint�t. - K�sz�tsetek egy met�dust, amely kilist�zza valamennyi m�dia
 * p�ld�nyt olyan m�don, hogy a felhaszn�l� ki tudja v�lasztani, hogy melyiket
 * �rt�keli. - K�sz�tsetek egy met�dust, ami lek�rdezi egy film/sorozat
 * �rt�kel�seit, �s kilist�zza az �tlagot.
 */